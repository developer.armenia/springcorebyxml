package am.egs.spring.xml.test.byautowired;

import am.egs.spring.xml.byautowired.University1;
import am.egs.spring.xml.byautowired.University2;
import am.egs.spring.xml.bysetter.Employee;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by haykh on 4/30/2019.
 */
public class ByAutowiredTest {
  ApplicationContext ctx;
  @Before
  public void initTest() {
    ctx = new ClassPathXmlApplicationContext("./beans_byautowired.xml");
  }

  @Test
  public void universityByNameTest() {
    University1 university1 = ctx.getBean("universityByName" , University1.class);
    Assert.assertNotNull(university1.getStudent());
    Assert.assertNull(university1.getWorker()); //not Autowired because no have worker name definition in bean.xml
    Assert.assertNull(university1.getOtherWorker()); //it is not Autowired because no have worker name definition in bean.xml
  }

  @Test
  public void universityByTypeTest() {
    University1 university1 = ctx.getBean("universityByType" , University1.class);
    Assert.assertNotNull(university1.getStudent());
    Assert.assertNotNull(university1.getWorker()); //autowired! BTW no have worker bean but have Worker definition type in bean.xml
    Assert.assertNotNull(university1.getOtherWorker()); //it is Autowired by type
  }

  @Test
  public void universityConstructorTest() {
    University2 university2 = ctx.getBean("universityConstructor", University2.class);
    Assert.assertNotNull(university2.getStudent());
    Assert.assertNotNull(university2.getWorker());
  }
}
