package am.egs.spring.xml.test.bysetter;

import am.egs.spring.xml.bysetter.Employee;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BySetterTest {
  ApplicationContext ctx;
  @Before
  public void initTest() {
    ctx =
        new ClassPathXmlApplicationContext("./beans_bysetter.xml");
  }
  @Test
  public void storeTest() {
    Employee employee = ctx.getBean(Employee.class);
    Assert.assertNotNull(employee);
  }
}
