package am.egs.spring.xml.bysetter;

public class Employee {
  private Department department;

  public Department getDepartment() {
    return department;
  }

  public void setDepartment(Department department) {
    this.department = department;
  }
}
