package am.egs.spring.xml.byconstructor;

public class StoreService {

  DatabaseConnection dbConnections;

  public StoreService(DatabaseConnection dbConnections){
    this.dbConnections = dbConnections;
  }

  public String showDBUrl(){
    return dbConnections.getUrl();
  }
}
